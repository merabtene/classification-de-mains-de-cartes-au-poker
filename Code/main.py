# -*- coding: UTF-8 -*-
#!/usr/bin/python


################################################################################################
#									Lecture du dataSet										   #
################################################################################################ 

def readDataSet(filename):
	inputs = []
	outputs= []
	f = open(filename)
	for ligne in f.readlines():
		S1,C1,S2,C2,S3,C3,S4,C4,S5,C5,OUTPUT= ligne.replace("\r\n","").split(",") 
		outputs.append( OUTPUT )
		inputs.append( (S1,C1,S2,C2,S3,C3,S4,C4,S5,C5) )
		f.close()
	return inputs,outputs

################################################################################################
#									CREATION DU MODEL										   #
################################################################################################
from sklearn import tree

def createDecisionTree():
	return tree.DecisionTreeClassifier()

################################################################################################
#									TRAINING DU MODEL										   #
################################################################################################
def trainModel(model,train_data,train_class):
	return model.fit(train_data,train_class)


#----------------------------------------------------------------------------------------------#
#										MAIN												   #
#----------------------------------------------------------------------------------------------#
from sklearn.metrics import precision_score, recall_score,accuracy_score,confusion_matrix,precision_recall_fscore_support
from   sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import graphviz 
import time 

inputs_train =[]
outputs_train=[]
print ("")
print ("\n\t\t\tDEBUT:\n\n")
plt.title('Mesures de performances')
plt.xlabel('Output')
plt.ylabel('Pourcentage')

# Separation des donnees (entrees et classe)
data = "poker-all.data"
inputs,outputs=readDataSet(data)

inputs = np.array(inputs)
outputs = np.array(outputs)
print("ENTREE        --> Nombre de lignes :  %d , Colonnes : %d" % inputs.shape) 
print("SORTIES       --> Nombre de lignes :  %d , Colonnes : 1\n" % outputs.shape[0]) 

test_size=0.20

tmp_debut=time.time()
inputs_train,inputs_test,outputs_train,outputs_test=train_test_split(inputs,outputs, test_size=test_size)
tmp_fin=time.time()-tmp_debut
print("Division: Train {}% , Test {}%".format(100-(test_size*100),test_size*100)) 
print("entrees_TRAIN --> Nombre de lignes :  %d , Colonnes : %d" % inputs_train.shape) 
print("sorties_TRAIN --> Nombre de lignes :  %d , Colonnes : 1" % outputs_train.shape[0])
print("entrees__TEST --> Nombre de lignes :  %d , Colonnes : %d" % inputs_test.shape) 
print("sorties_TEST  --> Nombre de lignes :  %d , Colonnes : 1" % outputs_test.shape[0])
print("Temps division: %fs\n"%tmp_fin)

# Creation du model
tmp_debut=time.time()
model =createDecisionTree()
tmp_fin=time.time()-tmp_debut
print ("Creation du model: terminée en %fs\n"%tmp_fin)

# Entrainement du model
tmp_debut=time.time()
trainModel(model,inputs_train,outputs_train)
tmp_fin=time.time()-tmp_debut
print ("Entrainement du model: terminé en %fs\n"%tmp_fin)


# Prediction
tmp_debut=time.time()
predictions = model.predict(inputs_test)
tmp_fin=time.time()-tmp_debut
print ("Prediction sur l'ensemble des tests: terminé en %fs\n"%tmp_fin)


# Mesures de performances
accuracy = accuracy_score(outputs_test,predictions)
print ("accuracy: {:.2%}.\n".format(accuracy)) # limiter %.2f

precision, recall, fscore, support =precision_recall_fscore_support(outputs_test,predictions, labels=np.unique(predictions))
print ("precision: {}.\n".format(precision))
print ("recall: {}.\n".format(recall))
print ("fscore: {}.\n".format(fscore))
print ("support: {}.\n".format(support))

matrice_confusion = confusion_matrix(outputs_test,predictions)
print ("matrice de confusion:\n {}.\n".format(matrice_confusion))


plt.plot(precision,label='Precision Arbre')
plt.plot(recall,label='Recall Arbre')



#**********************************************************************************************#
#									AVEC RANDOM FOREST MAINTENANT							   #
#**********************************************************************************************#

print("#***********************************************************************************************#\n")
print("#\t\t\t\t\tRANDOM FOREST MAINTENANT\t\t\t\t#\n")
print("#***********************************************************************************************#\n")

from sklearn.ensemble import RandomForestClassifier


nb_arbres = 50
print ("Nombre d'arbres : %d\n"%nb_arbres)
inputs_train,inputs_test,outputs_train,outputs_test=train_test_split(inputs,outputs)

# Creation de la foret
tmp_debut=time.time()
forest = RandomForestClassifier(n_estimators=nb_arbres)
tmp_fin=time.time()-tmp_debut
print ("Creation de la forêt: terminée en %fs\n"%tmp_fin)

# Entrainement de la foret
tmp_debut=time.time()
forest.fit(inputs_train,outputs_train)
tmp_fin=time.time()-tmp_debut
print ("Entrainement de la forêt: terminée en %fs\n"%tmp_fin)


# Prediction
tmp_debut=time.time()
predictions = forest.predict(inputs_test)
tmp_fin=time.time()-tmp_debut
print ("Predictions : terminé en %fs\n"%tmp_fin)

# Mesures de performances
accuracy = accuracy_score(outputs_test,predictions)
print ("accuracy: {:.2%}.\n".format(accuracy))

precision, recall, fscore, support =precision_recall_fscore_support(outputs_test,predictions, labels=np.unique(predictions))
print ("precision: {}.\n".format(precision))
print ("recall: {}.\n".format(recall))
print ("fscore: {}.\n".format(fscore))
print ("support: {}.\n".format(support))

matrice_confusion = confusion_matrix(outputs_test,predictions)
print ("matrice de confusion:\n {}.\n".format(matrice_confusion))


# Mesures de performances en graphe
plt.plot(precision,label='Precision Forest')
plt.plot(recall,label='Recall Forest')
plt.legend(loc="upper right")
plt.show()

print("\n\nFIN.") 





