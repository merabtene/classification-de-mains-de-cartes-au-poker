# -*- coding: UTF-8 -*-
#!/usr/bin/python


################################################################################################
#									Lecture du dataSet										   #
################################################################################################ 

def readDataSet(filename):
	inputs = []
	outputs= []
	f = open(filename)
	for ligne in f.readlines():
		S1,C1,S2,C2,S3,C3,S4,C4,S5,C5,OUTPUT= ligne.replace("\r\n","").split(",") 
		outputs.append( OUTPUT )
		inputs.append( (S1,C1,S2,C2,S3,C3,S4,C4,S5,C5) )
		f.close()
	return inputs,outputs

#----------------------------------------------------------------------------------------------#
#										MAIN												   #
#----------------------------------------------------------------------------------------------#

inputs =[]
outputs=[]
print ("")
print ("\n\t\t\tDEBUT:\n\n")

# Lecture et division du dataset (Separation des inputs et des outputs)
data = "poker-all.data"
inputs,outputs=readDataSet(data)
print ("Fin division des donnees.\n")

inputs_file = open("poker-inputs.data","w") 
outputs_file = open("poker-outputs.data","w") 

for ligne in inputs:
	inputs_file.write("{}\n".format(ligne).replace('(','').replace('\'','').replace(')',''))

for ligne in outputs:
	outputs_file.write("{}\n".format(ligne))
 
inputs_file.close() 
outputs_file.close() 



print("\n\nFIN.") 





